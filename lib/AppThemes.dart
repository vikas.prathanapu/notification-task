import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'size_config.dart';

enum AppThemeKeys{LIGHT,DARK,DARKER}

class AppThemes{

  static const Color primaryColorDark = Color(0xFF4d648c);
  static const Color primaryColor = Color(0xFF5D78A7);
  static const Color primaryColorLight = Color(0xFF7294cf);
  static const Color secondaryColor = Color(0xFFFFB44F);
  static const Color link = Color(0xFF666666);
  static const Color optional = Color(0xFF666666);
  static const Color terms = Color(0xFF666666);
  static const Color darkGrey = Colors.grey;
  static const Color urlLink = Colors.blueAccent;


  static const MaterialColor primarySwatch = const MaterialColor(
    0xFF0d47a1,
    const <int, Color>{
      50: const Color(0xFF0d47a1),
      100: const Color(0xFF0d47a1),
      200: const Color(0xFF0d47a1),
      300: const Color(0xFF0d47a1),
      400: const Color(0xFF0d47a1),
      500: const Color(0xFF0d47a1),
      600: const Color(0xFF0d47a1),
      700: const Color(0xFF0d47a1),
      800: const Color(0xFF0d47a1),
      900: const Color(0xFF0d47a1),
    },
  );

  static final ThemeData lightTheme = ThemeData(
    primaryColor: primaryColor,
    primaryColorLight: primaryColorLight,
    accentColor: secondaryColor,
    //primarySwatch: primarySwatch,
    brightness: Brightness.light,
    textTheme: lightTextTheme,
    backgroundColor: Colors.white
  );

  static final ThemeData darkTheme = ThemeData(
    primaryColor: Colors.grey,
    brightness: Brightness.dark,
    textTheme: lightTextTheme
  );

  static final ThemeData darkerTheme = ThemeData(
    primaryColor: Colors.black,
    brightness: Brightness.dark,
    textTheme: lightTextTheme
  );

  static final TextTheme lightTextTheme = TextTheme(
    headline: TextStyle(fontSize: SizeConfig.textMultiplier * 3.0),
    title: TextStyle(fontSize: SizeConfig.textMultiplier * 2.5),
    subtitle: TextStyle(fontSize: SizeConfig.textMultiplier * 2),
    body1: TextStyle(fontSize: SizeConfig.textMultiplier * 1.5),
    body2: TextStyle(fontSize: SizeConfig.textMultiplier * 1)
  );

  static final TextTheme darkTextTheme = TextTheme(
      headline: TextStyle(fontSize: SizeConfig.textMultiplier * 3.0),
      title: TextStyle(fontSize: SizeConfig.textMultiplier * 2.5),
      subtitle: TextStyle(fontSize: SizeConfig.textMultiplier * 2),
      body1: TextStyle(fontSize: SizeConfig.textMultiplier * 1.5),
      body2: TextStyle(fontSize: SizeConfig.textMultiplier * 1)
  );

  static final TextTheme darkerTextTheme = TextTheme(
      headline: TextStyle(fontSize: SizeConfig.textMultiplier * 3.0),
      title: TextStyle(fontSize: SizeConfig.textMultiplier * 2.5),
      subtitle: TextStyle(fontSize: SizeConfig.textMultiplier * 2),
      body1: TextStyle(fontSize: SizeConfig.textMultiplier * 1.5),
      body2: TextStyle(fontSize: SizeConfig.textMultiplier * 1)
  );

  static ThemeData getThemeFromKey(AppThemeKeys themeKey){
    switch(themeKey){
      case AppThemeKeys.LIGHT:
        return lightTheme;
      case AppThemeKeys.DARK:
        return darkTheme;
      case AppThemeKeys.DARKER:
        return darkerTheme;
      default:
        return lightTheme;
    }
  }

  static TextStyle formLabelStyle(BuildContext context){
    return Theme.of(context).textTheme.title.copyWith(fontWeight: FontWeight.normal,color: Colors.black87,fontSize: SizeConfig.textMultiplier * 2.2);
  }

  static TextStyle otpStyle(BuildContext context){
    return Theme.of(context).textTheme.title.copyWith(fontWeight: FontWeight.normal,color: AppThemes.primaryColor,letterSpacing: SizeConfig.widthMultiplier * 12);
  }

  static TextStyle otpPopupStyle(BuildContext context){
    return Theme.of(context).textTheme.title.copyWith(fontWeight: FontWeight.normal,color: AppThemes.primaryColor,letterSpacing: SizeConfig.widthMultiplier * 9);
  }

  static TextStyle requiredStyle(BuildContext context){
    return AppThemes.formLabelStyle(context).copyWith(color: AppThemes.secondaryColor);
  }

  static TextStyle textBoxStyle(BuildContext context){
    return Theme.of(context).textTheme.subtitle.copyWith(color: Colors.black87,fontWeight: FontWeight.normal,fontSize: SizeConfig.textMultiplier * 2);
  }

  static TextStyle textBoxDisabledStyle(BuildContext context){
    return Theme.of(context).textTheme.subtitle.copyWith(color: Colors.grey[500],fontWeight: FontWeight.normal,fontSize: SizeConfig.textMultiplier * 2);
  }

  static TextStyle textBoxHintStyle(BuildContext context){
    return Theme.of(context).textTheme.subtitle.copyWith(color: Colors.grey[400],fontWeight: FontWeight.normal,fontSize: SizeConfig.textMultiplier * 2);
  }

  static TextStyle cancelBoxHintStyle(BuildContext context){
    return Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontWeight: FontWeight.normal,fontSize: SizeConfig.textMultiplier * 1.5);
  }

  static TextStyle smallDesc(BuildContext context){
    return Theme.of(context).textTheme.body1.copyWith(fontSize: SizeConfig.textMultiplier * 1.2);
  }

  static TextStyle tabLabelStyle(BuildContext context){
    return Theme.of(context).textTheme.subtitle.copyWith(fontSize: SizeConfig.textMultiplier * 1.8,color: AppThemes.primaryColor);
  }

  static TextStyle tabUnSelectedLabelStyle(BuildContext context){
    return Theme.of(context).textTheme.subtitle.copyWith(fontSize: SizeConfig.textMultiplier * 1.8,color: Colors.grey);
  }

  static BoxDecoration loginTypeCircle(Color bgColor){
    return BoxDecoration(
      shape: BoxShape.circle,
      color: bgColor,
      border: Border.all(color: Color(0xFF5D77A6)),
      boxShadow: [BoxShadow(color: Colors.grey,blurRadius: 3.0,offset: Offset(0.0, 2.75))]
    );
  }

  static BoxDecoration textBoxDecoration(Color borderColor){
    return BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1)),
      border: Border.all(color: Colors.grey[300]),
      //boxShadow: [BoxShadow(color: Colors.grey,blurRadius: 1.0)],
      color: Colors.white
    );
  }

  static BoxDecoration disableTextBoxDecoration(Color borderColor){
    return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1)),
        //border: Border.all(color: borderColor),
        //boxShadow: [BoxShadow(color: Colors.grey,blurRadius: 3.0)],
        color: Colors.grey[300]
    );
  }

  static BoxDecoration borderLessBoxDecoration(){
    return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1)),
        border: Border.all(color: Colors.transparent),
        color: Colors.white
    );
  }

  static BoxDecoration borderContainerDecoration(Color color){
    return BoxDecoration(
      border: Border.all(color: color),
      borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1.8)),
    );
  }

  static BoxDecoration doneBoxDecoration(){
    return BoxDecoration(
      color: AppThemes.secondaryColor,
      borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1.8)),
    );
  }

  static BoxDecoration menuButtonDecoration(){
    return BoxDecoration(
      color: AppThemes.primaryColor,
      borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1.8)),
    );
  }

  static BoxDecoration rowBoxDecoration(){
    return BoxDecoration(
      color: Colors.white,
      border: Border.all(color: Colors.grey[300]),
      borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1.8))
    );
  }

  static BoxDecoration flatRowBoxDecoration(){
    return BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey[200]),
    );
  }
  static BoxDecoration flatRowBoxDecoration2(){
    return BoxDecoration(
      color: Colors.white,
      border: Border.all(color: Colors.blueAccent),
    );
  }

  static InputDecoration borderInputDecoration(String hint,TextStyle hintStyle){
    return InputDecoration(
        hintText: hint,
        hintStyle: hintStyle,
        contentPadding: EdgeInsets.symmetric(horizontal:SizeConfig.widthMultiplier * 2),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1.8)),
            borderSide: BorderSide(color: Colors.grey[300])
        ),
        disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1.8)),
            borderSide: BorderSide(color: Colors.grey[300])
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1.8)),
          borderSide: BorderSide(color: Colors.black87)
        ),
        counterText: ""
    );
  }

  static InputDecoration borderLessInputDecoration(String hint,TextStyle hintStyle){
    return InputDecoration(
        hintText: hint,
        hintStyle: hintStyle,
        border: InputBorder.none,
        //contentPadding: EdgeInsets.symmetric(horizontal:SizeConfig.widthMultiplier * 2),Color.fromRGBO(231, 226, 226,1)
        counterText: ""
    );
  }

  static InputDecoration cancelInputDecoration(String hint,TextStyle hintStyle){
    return InputDecoration(
        hintText: hint,
        hintStyle: hintStyle,
        border: InputBorder.none,
        //contentPadding: EdgeInsets.symmetric(horizontal:SizeConfig.widthMultiplier * 2),
        counterText: ""
    );
  }

  static InputDecoration disableInputDecoration(String hint,TextStyle hintStyle){
    return InputDecoration(
        hintText: hint,
        hintStyle: hintStyle,
        filled: true,
        fillColor: Colors.grey[300],
        contentPadding: EdgeInsets.symmetric(horizontal:SizeConfig.widthMultiplier * 2),
        disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 1.8)),
            borderSide: BorderSide(color: Colors.grey[300])
        ),
        counterText: ""
    );
  }



  static RoundedRectangleBorder buttonShpae(){
    return RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(SizeConfig.widthMultiplier * 5)),
    );
  }

  static showToast(String msg,Toast length,ToastGravity gravity){
    Fluttertoast.showToast(
        msg: msg,
        toastLength: length,
        gravity: gravity,
        backgroundColor: AppThemes.secondaryColor,
        textColor: Colors.white,
        fontSize: 12.0
    );
  }

}