import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:testnotification/popup.dart';
import 'AppThemes.dart';
import 'package:intl/intl.dart';

class FullScreen extends StatefulWidget {
  @override
  _ExecutiveDashboardState createState() => _ExecutiveDashboardState();
}

class _ExecutiveDashboardState extends State<FullScreen> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<String> id=["#293","#293","#UK-HYD-0122"];
  List<int> mobile=[999999999,9999999999,9999999999];
  List<String> _images=["assets/images/download.png","assets/images/ic_order1.png","assets/images/ic_order1.png","assets/images/ic_order2.png"];
  List<String> address=["fbakdfklsjadhfksjfhksaljdfhkjsfn,smcnzbkjfhslfdk;adlkfnzcmzn,xmfsjkdhfkal","fbakdfklsjadhfksjfhksaljdfhkjsfn,smcnzbkjfhslfdk;adlkfnzcmzn,xmfsjkdhfkal","fbakdfklsjadhfksjfhksaljdfhkjsfn,smcnzbkjfhslfdk;adlkfnzcmzn,xmfsjkdhfkal",];
  List<String> fstatus=["New Order","Pending","Pending"];
  List<String> status=["All","New Order","Pending"];
  List<int> count=[7,7,7];
  List<int> cancel=[0,1,0,0,1,1,0];
  List<int> cost=[587,587,587];
  List<int> fmonth=[5,6,7];
  List<String> month=["All","Jan","Feb",];
  List<String> dte=["All","1","2"];
  String _dte;
  String _month;
  String _status="All";

  String convertDateTimeDisplay(String date) {
    final DateFormat displayFormater = DateFormat('yyyy-MM-dd HH:mm:ss.SSS');
    final DateFormat serverFormater = DateFormat('dd-MM-yyyy');
    final DateTime displayDate = displayFormater.parse(date);
    final String formatted = serverFormater.format(displayDate);
    return formatted;
  }

  @override


  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
          child:SingleChildScrollView(
            child: Container(
              color: AppThemes.primaryColorDark,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text("Time: 01:59",style: TextStyle(color: Colors.white,fontSize: 18),)
                      ],

                    ),
                  ),
                  SizedBox(height: 20,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 150,
                        width: 150,
                        decoration:BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/images/ic_intro_one_stop_shop.png"
                            )
                          )
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 5,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("New Order Received !!",style: TextStyle(color: AppThemes.secondaryColor,fontSize: 30),)
                    ],
                  ),
              Padding(
                padding: EdgeInsets.all(15),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: AppThemes.secondaryColor,
                        width: 2,
                      )
                  ),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width:280,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        width: 100,
                                        child: Row(
                                          children: <Widget>[
                                            Text(" ORDER ID: ",style: Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontSize: 13),textAlign: TextAlign.left),
                                            Text(id[1],style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.black,fontWeight: FontWeight.normal,fontSize: 13),textAlign: TextAlign.left),

                                          ],
                                        ),
                                      ),
                                      Container(
                                        width: 120,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Text("Delivery Address:",style: Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontSize: 13),),
                                            Text(address[1],style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.black,fontWeight: FontWeight.normal,fontSize: 13),),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15,left: 15,right: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(" Total Items: ",style: Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontSize: 13),textAlign: TextAlign.left),
                            Text("4",style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.black,fontWeight: FontWeight.normal,fontSize: 13),textAlign: TextAlign.left),
                          ],
                        ),
                      ),
                      Container(
                        width: 280,
                        height: 100,
                        child: ListView.separated(
                            separatorBuilder: (BuildContext context,int index){
                              return SizedBox(width:6);
                            },
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: 4,
                            itemBuilder: (BuildContext context, int index){
                              return Column(
                                children: <Widget>[
                                  Container(
                                    height: 50,
                                    width: 50,
                                    decoration: BoxDecoration(
                                        borderRadius:BorderRadius.all(Radius.circular(10)),
                                        image: DecorationImage(
                                            image: AssetImage("assets/images/download.png"),
                                            fit: BoxFit.fill
                                        )
                                    ),
                                  ),
                                  SizedBox(height: 4,),
                                  Text("Chana dal",style: Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontWeight: FontWeight.normal,fontSize: 8),),
                                  SizedBox(height: 10,),
                                  Text("Qty: 1",style: Theme.of(context).textTheme.subtitle.copyWith(fontSize: 8),)
                                ],
                              );
                            }
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 7,left: 15,right: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(" Order Summary ",style: Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontSize: 13),textAlign: TextAlign.left),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 7,left: 15,right: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("  Total Items:2",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,),
                            Text("Rs 73.51",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,)
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 7,left: 15,right: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("  Delivery Charges",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,),
                            Text("Free",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,)
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 7,left: 15,right: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("  GST applicable:18%",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,),
                            Text("Rs 06.49",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,)
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 7,left: 5,right: 5),
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                            color: AppThemes.primaryColorLight,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(0.0, 1.0), //(x,y)
                                blurRadius: 6.0,
                              ),
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(top:7,bottom: 7,left: 15,right: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("  Total",style:Theme.of(context).textTheme.subtitle.copyWith(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold) ,),
                                Text("Rs 80.00",style:Theme.of(context).textTheme.subtitle.copyWith(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold) ,)
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 25,)
                     ],
                  ),
                ),
              ),
                  Padding(
                      padding:EdgeInsets.only(top: 20,bottom: 40,left: 60,right: 60),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            GestureDetector(
                              child: Container(
                                  height: 50,
                                  width: 50,
                                  color: Colors.green,
                                  child: Icon(Icons.check,color: Colors.white,size:30,)
                              ),
                              onTap: (){
                                showDialog(context: context, builder:(context){
                                  return PopUp();
                                });
                              },
                            ),
                            SizedBox(height: 10,),
                            Text("Approve",style:Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.secondaryColor,fontWeight: FontWeight.bold,fontSize: 15),)
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                                height: 50,
                                width: 50,
                                color: Colors.red,
                                child: Icon(Icons.close,color: Colors.white,size:30,)
                            ),
                            SizedBox(height: 10,),
                            Text("Reject",style:Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.secondaryColor,fontWeight: FontWeight.bold,fontSize: 15),)

                          ],
                        )
                      ],
                    ),
                  ),
                  Text("Order will get auto cancelled if not approved in 2 minutes.",style: TextStyle(color: Colors.white),),
                  SizedBox(height: 10,),
                ],
              ),
            ),
          )
      ),
    );
  }
}
