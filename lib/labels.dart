class Labels{
  static final String appTitle = "UrbanKart";
  static final String introTitle1 = "One Stop Shop";
  static final String introDesc1 = "Single Application for selling livelihood products";
  static final String introTitle2 = "Sell your products Online";
  static final String introDesc2 = "Online Platform to increase your sales";
  static final String introTitle3 = "Receive Online Payments";
  static final String introDesc3 = "Get your payments directly transffered to your bank account";
  static final String mobileNumber ="Mobile Number";
  static final String mobileNumberHint = "XXXXXXXXXX";
  static final String termsAndConditions = "Please read & agree to the ";
  static final String termsAndConditionsLink = " privacy policy";
  static final String emailId = "Email ID";
  static final String emailHint ="Enter email id";
  static final String password = "Password";
  static final String passwordHint ="Enter password";
  static final String confirmPassword = "Confirm Password";
  static final String confirmPasswordHint = "Enter confirm password";
  static final String enterOtp = "Enter OTP";
  static final String enterOtpHint = "  Enter OTP";
  static final String resendOtp = "Resend OTP";
  static final String newTo = "New to UrbanKart ?";
  static final String alreadyRegistered = "Already Sign Up to UrbanKart ?";
  static final String registerOTPTerm = "We will send a One Time Password (OTP) on given mobile number for verification.";
  static final String confirm = "CONFIRM";
  static final String signUp = "Sign Up";
  static final String signIn = "Sign In";
  static final String register = "REGISTER";
  static final String addYourLogo = "Add your Logo here";
  static final String addYourLogoDesc = "Users will be able to see your logo while choosing products";
  static final String addServiceLogo = "Add service logo here";
  static final String addServiceLogoDesc = "Users will be able to see your logo while choosing products";
  static final String addCategoryLogo = "Add category logo here";
  static final String addCategoryLogoDesc = "Users will be able to see your logo while choosing products";
  static final String businessName = "Business Name";
  static final String businessNameHint = "Enter business name";
  static final String businessNameInfo = "( Customers will be able to see your shop name )";
  static final String contactPerson = "Contact Person";
  static final String contactPersonHint = "Enter contact person name";
  static final String whatsAppNumber = "(WhatsApp Number for support)";
  static final String serviceType = "Service Type";
  static final String otherOptions = " Other Login Options ";
  static final String phone = "Phone";
  static final String google = "Google";
  static final String facebook = "Facebook";
  static final String apple = "Apple";
  static final String email = "Email";
  static final String forgotPassword = "Forgot Password";
  static final String location = "Area / Locality";
  static final String locationHint = "Enter Area OR Locality";
  static final String community = "Society / Community";
  static final String communityHint = "Enter Society OR Community";
  static final String street = "Street";
  static final String streetHint = "Enter Street";
  static final String state = "State";
  static final String stateHint = "Enter state";
  static final String pincode = "Pincode";
  static final String pincodeHint = "Enter pincode";
  static final String verifyPhoneNumberHelp = "Please verify your phone number before completing registration.";
  static final String registerNewLocation = "Register New Location";
  static final String registerNewCommunity = "Register New Community";
  static final String communityApartment = "Community/ Aparment/ Society Name";
  static final String communitySearchHint = "Name of your apartment/ Community";
  static final String request = "Request";
  static final String areaOrLocation ="Area/ Location";
  static final String locationReqHint = "Enter area,location,.. OR Postal Code";
  static final String locReqConfirmMsg ="Your wish request has been registered with us. We shall come to your location soon.";
  static final String comReqConfirmMsg = "Your wish request has been registered with us. We shall come to your community soon.";
  static final String thanks = "Thanks for your Interest!";
  static final String selectServiceTypeHint = "Select service type(s)";
  static final String attachProof = "Attach Proof";
  static final String signUpDone = "Registration Completed";
  static final String signUpPending = "Registration Pending";
  static final String signUpRejected = "Registration Rejected";
  static final String signUpDoneMsg1 = "Thank you for registering with Urban Kart as a vendor. Please download the agreement attached along with T&C and upload the signed copy hereunder.";
  static final String signUpDoneMsg2 = "Once we receive the signed document PDF, you shall gain access to admin dashboard in 48 hours.";
  static final String downloadAgreement = "Download Agreement";
  static final String uploadSignedAgreement = "Upload Signed Agreement";
  static final String accountApproved = "Account Approved";
  static final String titleAdmin = "UrbanKart Admin";
  static final String titleVendor = "UrbanKart Vendor";
  static final String titleExecutive = "UrbanKart Executive";
  static final String manageLocations = "Manage Locations";
  static final String manageServices = "Manage Services";
  static final String manageBanners = "Manage Banners";
  static final String manageCategories = "Manage Categories";
  static final String manageExecutive = "Manage Executives";
  static final String manageProducts = "Manage Products";
  static final String manageOffers = "Manage Offers";
  static final String manageVendors = "Manage Vendors";
  static final String addService = "Add New Service";
  static final String updateService = "Update Service";
  static final String serviceName = "Service Name";
  static final String serviceNameHint = "Enter service name";
  static final String serviceStatus = "Service Status";
  static final String addCategory = "Add New Category";
  static final String updateCategory = "Update Category";
  static final String categoryName = "Category Name";
  static final String categoryNameHint = "Enter category name";
  static final String categoryStatus = "Category Status";
  static final String categoryServiceName = "Category Service";
  static final String add = "Add";
  static final String update = "Update";
  static final String edit = "Edit";
  static final String delete = "Delete";
  static final String rejectReason = "Reject Reason";
  static final String rejectReasonHint = "Enter Reason";
  static final String newProduct = "New Product";
  static final String essentialsType = "Essentials Type";
  static final String categoryType = "Category Type";
  static final String productName = "Product Name";
  static final String productNameHint = "Enter product name";
  static final String productDescription = "Prodcut Description";
  static final String productDescriptionHint = "Enter prodcut description";
  static final String selectUnit = "Select Unit";
  static final String avaiablePacking = "Available Packing";
  static final String unitPrice = "Unit Price";
  static final String unitPriceHint = "Enter unit price";
  static final String targetDeliveryTime = "Target Delivery Time";
  static final String stockStatus = "Stock Status";
  static final String outOfStock = "Out Of Stock";
  static final String available = "Available";
  static final String createProduct = "Create Product";
  static final String updateProduct = "Update Product";
  static final String addProductLogo = "Add product logo here";
  static final String addProductLogoDesc = "Users will be able to see your logo while choosing products";
  static final String productUnitWisePrice = "Unit Wise Price List";
  static final String mrpHint = "Enter MRP";
  static final String sellingPriceHint = "Enter price";
  static final String customInput = "Custom Packing";
  static final String customInputHint = "Enter Custom value";
  static final String searchForTheImage = "Search for the image";
  static final String addNewImage = "Add New Image";
  static final String addNewImageDesc = "If you request for new image upload, review by UrbanKart team will be required.";
  static final String uploadImage = "Upload Image";
  static final String imageName = "Image Name";
  static final String imageNameHint = "Enter image name";
  static final String requestApproval = "Request Approval";
  static final String addNewCommunity = "Add New Community";
  static final String completeAddress = "Complete Address";
  static final String bestOffer = "Best Offer";
  static final String enable = "Enable";
  static final String disable = "Disable";


  static final String agreementTitle = "UrbanKart Vendor Agreement";
  static final String lastUpdated = "Last updated: March 2020";
  static final String generalTerms = "General Terms";
  static final String generalTermPara1 = """This agreement is between "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _" and "IoTMinds" that
governs your access to and use of the services on the UrbanKart mobile application
through a particular vendor account. Both the parties here agree to be bounded by the
terms of this agreement mentioned below for use in connection with the UrbanKart
Mobile application. As used in this Agreement, "we," "us," and "UrbanKart" means the
IoTMinds company named in the applicable Terms.""";
  static final String enrollmentTitle = "1. Enrolment";
  static final String enrollmentPara1 = """To begin the enrolment process, you must complete the registration process for one or
more services in the UrbanKart Mobile application. Use of the Services is limited to
parties that can lawfully enter into and form contracts under applicable Law. As part of
the application, you must provide us with your (or your business') legal name, address,
phone number, e-mail address, GST registration details as well as any other information
we may request. Any personal data you provide to us will be handled in accordance
with UrbanKart privacy policies.""";
  static final String serviceFeePaymentsTitle = "2. Service Fee Payments";
  static final String serviceFeePaymentsPara1 = """Fee details are described fully in the applicable Service Terms and Program Policies.
You are responsible for all of your expenses in connection with this Agreement, unless
this Agreement or the applicable Service Terms provide otherwise. You will use only a
name you are authorized to use in connection with the Services and will update such
information as necessary to ensure that it at all times remains accurate and complete.
You authorize us to verify your information (including any updated information).At
UrbanKart's vendor payment option, all payments have to be made using online
payment options. Also, UrbanKart shall settle all the payments due if any at the end of
every month i.e.30 th of every month. You agree that UrbanKart shall not be liable for any
failure to make payments to you on account of incomplete or inaccurate information
provided by you with respect to Your user profile.""";
  static final String serviceFeePaymentsPara2 = """In addition to charging payable sums, we may instead choose to either (a) offset any
amounts that are payable by you to us (in reimbursement or otherwise) against any
payments we may make to you, or (b) invoice you for amounts due to us, in which case
you will pay the invoiced amounts upon receipt. Except as provided otherwise, all
amounts contemplated in this Agreement will be expressed and displayed in the Local
Currency, and all payments contemplated by this Agreement will be made in the Local
Currency. If we discover erroneous or duplicate transactions, then we reserve the right
to seek reimbursement from you by deducting from future payments owed to you,
seeking such reimbursement from you by any other lawful means; provided that the
foregoing will not limit your rights to pursue any good faith dispute with UrbanKart
concerning whether any amounts are payable or due.""";
  static final String serviceFeePaymentsPara3 = """If we reasonably conclude based on information available to us that your actions and/or
performance in connection with the Agreement may result in a significant number of
customer disputes, chargebacks or other claims in connection with the UrbanKart mobile
application, then we may, in our sole discretion and subject to applicable Law, delay
initiating any payments to be made or that are otherwise due to you under this
Agreement for the shorter of: (a) a period of ninety (90) calendar days following the initial
date of suspension; or (b) completion of any investigation(s) regarding your actions
and/or performance in connection with the Agreement. You agree that we are entitled to
the interest, if any, paid on balances maintained as deposits in our bank accounts.""";
  static final String iHearBy = """I hereby solemnly declare my acknowledgment and approval to above mentioned terms
& conditions.""";
  static final String authorized = "Authorized Signatures :";
  static final String vendorDetails = "Vendor Details :";
  static final String vendorName = "Name :";
  static final String vendorPhoneNumber = "Phone Number :";
  static final String vendorBusinessName = "Business Name :";
  static final String vendorAddress = "Address:";




}