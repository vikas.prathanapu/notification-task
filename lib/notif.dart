import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:testnotification/fullscreen.dart';
import 'AppThemes.dart';
import 'dart:async';

import 'package:testnotification/popup.dart';

class Notif extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Notif> {
  String textValue = 'Hello World !';
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();

  @override
  void initState() {
    super.initState();

    var android = new AndroidInitializationSettings('mipmap/ic_launcher');
    var ios = new IOSInitializationSettings();
    var platform = new InitializationSettings(android, ios);
    flutterLocalNotificationsPlugin.initialize(platform);

    firebaseMessaging.configure(
      // ignore: missing_return
      onLaunch: (Map<String, dynamic> msg) {
        showDialog(context: context, builder:(context){
          return PopUp();
        });
      },
      // ignore: missing_return
      onResume: (Map<String, dynamic> msg) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => FullScreen()));
      },
      // ignore: missing_return
      onMessage: (Map<String, dynamic> msg) {
        showNotification(msg);
        print(" onMessage called ${(msg)}");
      },
    );
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, alert: true, badge: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings setting) {
      print('IOS Setting Registed');
    });
    firebaseMessaging.getToken().then((token) {
      update(token);
    });
  }

  showNotification(Map<String, dynamic> msg) async {
    var android = new AndroidNotificationDetails(
      'sdffds dsffds',
      "CHANNLE NAME",
      "channelDescription",
    );
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android, iOS);
    await flutterLocalNotificationsPlugin.show(
        0, "This is title", "this is demo", platform);
  }

  update(String token) {
    print(token);
    DatabaseReference databaseReference = new FirebaseDatabase().reference();
    databaseReference.child('fcm-token/${token}').set({"token": token});
    textValue = token;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      color: Colors.white,
      child: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 410,),
            FlatButton(
              onPressed: null,
              child: Container(
                color:AppThemes.primaryColorDark,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("Place Order",style: TextStyle(color: Colors.white),),
                ),
              ),
            ),
            FlatButton(
              onPressed:() async{
              },
              child: Container(
                color:AppThemes.primaryColorDark,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("Register for Notification",style: TextStyle(color: Colors.white),),
                ),
              ),
            ),
            FlatButton(
              onPressed:() async{
              },
              child: Container(
                color:AppThemes.primaryColorDark,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text(textValue ,style: TextStyle(color: Colors.white),),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}