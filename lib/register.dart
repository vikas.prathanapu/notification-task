import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:testnotification/popup.dart';
import 'AppThemes.dart';
import 'package:intl/intl.dart';

class Register extends StatefulWidget {
  @override
  _ExecutiveDashboardState createState() => _ExecutiveDashboardState();
}

class _ExecutiveDashboardState extends State<Register> {
  Future<void> addData(_user) async {
    Firestore.instance
        .collection('User Details')
        .document(_user['uid'])
        .setData(_user)
        .catchError((e) {
      print(e);
    });
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: MediaQuery.of(context).size.height,
      color: Colors.white,
      child: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 410,),
            FlatButton(
              onPressed: null,
              child: Container(
                color:AppThemes.primaryColorDark,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("Place Order",style: TextStyle(color: Colors.white),),
                ),
              ),
            ),
            FlatButton(
              onPressed:() async{
                final Map<String, dynamic> UserDetails = {
                  'uid': 1,
                };
                addData(UserDetails);
              },
              child: Container(
                color:AppThemes.primaryColorDark,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("Register for Notification",style: TextStyle(color: Colors.white),),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}
