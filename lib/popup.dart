import 'package:flutter/material.dart';
import 'labels.dart';
import 'AppThemes.dart';
import 'size_config.dart';

class PopUp extends StatefulWidget {
  @override
  _MobileVerificationDialogState createState() => _MobileVerificationDialogState();
}

class _MobileVerificationDialogState extends State<PopUp> {
  List<String> id=["#293","#293","#UK-HYD-0122"];
  List<int> mobile=[999999999,9999999999,9999999999];
  List<String> _images=["assets/images/download.png","assets/images/ic_order1.png","assets/images/ic_order1.png","assets/images/ic_order2.png"];
  List<String> address=["fbakdfklsjadhfksjfhksaljdfhkjsfn,smcnzbkjfhslfdk;adlkfnzcmzn,xmfsjkdhfkal","fbakdfklsjadhfksjfhksaljdfhkjsfn,smcnzbkjfhslfdk;adlkfnzcmzn,xmfsjkdhfkal","fbakdfklsjadhfksjfhksaljdfhkjsfn,smcnzbkjfhslfdk;adlkfnzcmzn,xmfsjkdhfkal",];
  List<int> check=[0,0,1,1];
  TextEditingController textController = TextEditingController();
  FocusNode textFocus = FocusNode();
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 2.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(
            color: AppThemes.secondaryColor,
            width: 2,
          )
      ),
      child: Container(
        height: 385,
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width:220,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(" ORDER ID: ",style: Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontSize: 13),textAlign: TextAlign.left),
                                Text(id[1],style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.black,fontWeight: FontWeight.normal,fontSize: 13),textAlign: TextAlign.left),

                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15,left: 15,right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(" Total Items: ",style: Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontSize: 13),textAlign: TextAlign.left),
                  Text("4",style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.black,fontWeight: FontWeight.normal,fontSize: 13),textAlign: TextAlign.left),
                ],
              ),
            ),
            Container(
              width: 280,
              height: 100,
              child: ListView.separated(
                  separatorBuilder: (BuildContext context,int index){
                    return SizedBox(width:6);
                  },
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: 4,
                  itemBuilder: (BuildContext context, int index){
                    return Column(
                      children: <Widget>[
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                              borderRadius:BorderRadius.all(Radius.circular(10)),
                              image: DecorationImage(
                                  image: AssetImage("assets/images/download.png"),
                                  fit: BoxFit.fill
                              )
                          ),
                        ),
                        SizedBox(height: 4,),
                        Text("Chana dal",style: Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontWeight: FontWeight.normal,fontSize: 8),),
                        SizedBox(height: 10,),
                        Text("Qty: 1",style: Theme.of(context).textTheme.subtitle.copyWith(fontSize: 8),)
                      ],
                    );
                  }
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7,left: 15,right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(" Order Summary ",style: Theme.of(context).textTheme.subtitle.copyWith(color: AppThemes.primaryColorLight,fontSize: 13),textAlign: TextAlign.left),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7,left: 15,right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("  Total Items:2",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,),
                  Text("Rs 73.51",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,)
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7,left: 15,right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("  Delivery Charges",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,),
                  Text("Free",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,)
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7,left: 15,right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("  GST applicable:18%",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,),
                  Text("Rs 06.49",style:Theme.of(context).textTheme.subtitle.copyWith(fontSize: 10,fontWeight: FontWeight.normal) ,)
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 7,left: 5,right: 5),
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                  color: AppThemes.primaryColorLight,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 1.0), //(x,y)
                      blurRadius: 6.0,
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top:7,bottom: 7,left: 15,right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("  Total",style:Theme.of(context).textTheme.subtitle.copyWith(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold) ,),
                      Text("Rs 80.00",style:Theme.of(context).textTheme.subtitle.copyWith(color:Colors.white,fontSize: 20,fontWeight: FontWeight.bold) ,)
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top:7,bottom: 7,left: 15,right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(onPressed: null,
                      child: Container(
                        height: 30,
                        width: 60,
                        color: Colors.green,
                        child: Center(child: Text("Accept",style: TextStyle(color: Colors.white,fontSize: 12),)),
                      )
                  ),
                  FlatButton(onPressed: null,
                      child: Container(
                        height: 30,
                        width: 60,
                        color: Colors.red,
                        child: Center(child: Text("Reject",style: TextStyle(color: Colors.white,fontSize: 12),)),
                      )
                  )
                ],
              )
            )
          ],
        ),
      ),
    );
  }
}
